#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "probleme.h"
#include "simplexe.h"
#include "pivot.h"

int main (int argc, char *argv[]) {
	char nomFichier[20];
	prob_t probleme = {0,0,0,NULL,NULL,NULL,NULL};

	strcpy(nomFichier, argv[1]);

	if (!lireProbleme(nomFichier, &probleme)) {
		afficherProbleme(probleme);
		double** rainbow;
		rainbow = initMatPivot(probleme);
		afficherMatPoivrot(rainbow, probleme);
		int col, ligne;
		col = selectionnerColPoivrot(rainbow, probleme);
		ligne = selectionnerLignePoivrot(rainbow, probleme, col);
		diviserLignePoivrot(rainbow, probleme, col, ligne);
		afficherMatPoivrot(rainbow, probleme);
		
	}
	else {
		printf("Probleme lecture Probleme\n");
		return -1;
	}

	libererMemoireProbleme(probleme);

	return 0;
}
