/*A completer*/
#include "pivot.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

double ** initMatPivot(prob_t probleme){
	int i, j;
	double ** matrice;
	matrice = (double**) malloc(sizeof(double*) * (probleme.nCont +1));
	for(i=0; i<= probleme.nCont; i++)
		matrice[i] = (double*) malloc(sizeof(double) * (probleme.nVar + probleme.nCont));
	
	for(i=0; i< probleme.nCont; i++){
		for(j=0; j<= probleme.nVar; j++){
			matrice[i][j] = probleme.cont[i][j];
		}
		for(j=0; j< probleme.nCont; j++){
			if(j != i)
				matrice[i][probleme.nVar +j] = 0;
			else
				matrice[i][probleme.nVar +j] = 1;
		}
		matrice[i][probleme.nVar + probleme.nCont] = probleme.valCont[i];
	}
	for(i=0; i< probleme.nVar; i++)
		matrice[probleme.nCont][i] = probleme.fonc[i];
	return matrice;
}

void afficherMatPoivrot(double** rainbow, prob_t probleme){
	printf("afficher rainbow : \n");
	int i, j;
	for(i=0; i <= probleme.nCont; i++){
		for(j=0; j<= probleme.nCont + probleme.nVar; j++){
			printf(" %6.2lf ", rainbow[i][j]);
		}
		printf("\n");
	}
	
}


int selectionnerColPoivrot(double** rainbow, prob_t probleme){
	int i, max = INT_MIN, indice = 0;
	for(i=0; i< probleme.nVar; i++){
		if(max< rainbow[probleme.nCont][i]){
			max = rainbow[probleme.nCont][i];
			indice = i;
		}
	}
	return indice;
}

int selectionnerLignePoivrot(double** rainbow, prob_t probleme, int col){
	int i, indice=0; 
	double min=INT_MAX;
	double resultat;
	for(i=0; i< probleme.nCont; i++){
		if(rainbow[i][col]==0)
			printf("error");
		else
			resultat = rainbow[i][probleme.nCont+probleme.nVar] / rainbow[i][col];
		if(min > resultat){
			min = resultat;
			indice = i;
		}
	}
	return indice;
}

void diviserLignePoivrot(double** rainbow, prob_t probleme, int col, int ligne){
	int i;
	double div;
	div = rainbow[ligne][col];
	printf("%f\n", div);
	for(i=0; i<= probleme.nVar + probleme.nCont; i++){
		rainbow[ligne][i] = rainbow[ligne][i]/div;
	}
}

void miseAZeroColPivot(double** rainbow, prob_t probleme, int col, int ligne){
	int i;
	for(i=0; i< probleme
}

/**************************************/
